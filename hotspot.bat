rem  Create a hostpot network with cmd
rem  REQUIREMENTS:
rem  - A WiFi adapter must be available
rem  - LAN network must share connection with the new Hostpot
@echo off
netsh wlan stop hostednetwork
netsh wlan set hostednetwork mode=allow ssid=MyHostpot key=myp455w0rd
netsh wlan start hostednetwork
rem  Create a log file for verify that network was created
netsh wlan show hostednetwork > hotspot.log
